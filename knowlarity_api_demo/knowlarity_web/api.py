# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

# Third Party Stuff
from .models import QuestionModel
from .serializers import QuestionModelSerializer
from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
import requests
import json


class ComplaintViewSet(generics.UpdateAPIView, generics.DestroyAPIView, generics.CreateAPIView):

    """
    API endpoint that allows user to submit their complaints
    """
    permission_classes = (AllowAny, )
    serializer_class = QuestionModelSerializer
    queryset = QuestionModel.objects.all()

    def post(self, request, format=None):
        # Compare the Input Data & Stored Question for Semantic Relatedness
        # Out of the existing one's whosoever's semantic relatedness is high
        # Put that question and it's relevant answer out.
        user_input = "Stephen Hawking is theoretical physicist within the University of Cambridge"
        result_score_list = []
        for question_qu in QuestionModel.objects.all():

            url = "https://dev.neuronme.com/api/semanticrelatedness/"
            print(str(question_qu.question_statement))
            payload = {"text": user_input,
                       "query": [str(question_qu.question_statement)]}
            headers = {
                'content-type': "application/json",
                'authorization': "Token d9143b56440feb5dd09197c6bd64c34542b57b4e",
            }
            status = requests.post(
                url, data=json.dumps(payload), headers=headers)
            result = status.json()
            print(result['scores'][0]['score'])
            result_score_list.append(result)
            print(result_score_list)
            # extracting query with maximum match in database
            # result_score = max(result, key=lambda x: x.get('score', 0))
            # import ipdb; ipdb.set_trace()
        return Response(result_score_list)
