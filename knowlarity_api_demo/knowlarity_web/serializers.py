from rest_framework import serializers

from . import models


class QuestionModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.QuestionModel
        fields = ('question_statement', 'answer_field', 'topic_question')
