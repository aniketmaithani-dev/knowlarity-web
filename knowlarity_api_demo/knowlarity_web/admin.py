# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import QuestionModel


class QuestionModelAdmin(admin.ModelAdmin):
    """ Status Listner Admin """
    list_display = ("question_statement", "answer_field",
                    "topic_question",)
    list_filter = ("question_statement",)
    search_field = ["question_statement", "answer_field",
                    "topic_question", ]


admin.site.register(QuestionModel, QuestionModelAdmin)
