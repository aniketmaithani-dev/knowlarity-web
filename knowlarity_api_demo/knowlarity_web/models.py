# -*- coding: utf-8 -*-

# Third Party Stuff
from django.db import models


class QuestionModel(models.Model):
    """
    Model to store Q&A Information
    """
    question_statement = models.CharField(max_length=255, blank=False)
    answer_field = models.TextField(blank=False)
    topic_question = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.question_statement
